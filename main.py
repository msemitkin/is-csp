from constraints import *
from data import get_lessons, get_days
from solutions import *
from prettytable import PrettyTable


def print_solution(solution):
    table = PrettyTable()
    table.field_names = ["Day", "Discipline", "Teacher"]
    rows = [[solution.get(key), key.name, key.teacher] for key in solution]
    rows.sort(key=lambda row: row[0])
    table.add_rows(rows)
    print(table)


lessons = get_lessons()
days = get_days()

normal_solution = normal(lessons, days, distribution_constraint, teachers_constraint, lessons_constraint)
backtrack_solution = backtrack(lessons, days, distribution_constraint, teachers_constraint, lessons_constraint)
rec_backtrack_sol = recursive_backtrack(lessons, days, distribution_constraint, teachers_constraint, lessons_constraint)
min_conflicts_solution = min_conflicts(lessons, days, distribution_constraint, teachers_constraint, lessons_constraint)

print("\nFinal Schedules:")
print("\nNormal")
print_solution(normal_solution)
print("\nBacktrack")
print_solution(backtrack_solution)
print("\nRecursive backtrack")
print_solution(rec_backtrack_sol)
print("\nMin conflicts")
print_solution(min_conflicts_solution)
