from models import Lesson, Teacher


def get_lessons():
    return [
        Lesson("Human Rights", Teacher("Teacher A")),
        Lesson("Business Management", Teacher("Teacher A")),
        Lesson("Intelligent Systems", Teacher("Teacher B")),
        Lesson("Calculus", Teacher("Teacher B")),
        Lesson("AI Models", Teacher("Teacher B")),
        Lesson("Cyber Security", Teacher("Teacher C")),
        Lesson("Math", Teacher("Teacher C")),
        Lesson("Physics", Teacher("Teacher C")),
        Lesson("Foreign language", Teacher("Teacher C")),
        Lesson("Operational systems", Teacher("Teacher D")),
        Lesson("BI Systems", Teacher("Teacher D"))
    ]


def get_days():
    return [1, 2, 3, 4, 5]
