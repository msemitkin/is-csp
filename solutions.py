from constraint import Problem, MinConflictsSolver, BacktrackingSolver, RecursiveBacktrackingSolver


def normal(lessons, days, *constraints):
    return get_solution(None, lessons, days, *constraints)


def min_conflicts(lessons, days, *constraints):
    return get_solution(MinConflictsSolver(), lessons, days, *constraints)


def recursive_backtrack(lessons, days, *constraints):
    return get_solution(RecursiveBacktrackingSolver(), lessons, days, *constraints)


def backtrack(lessons, days, *constraints):
    return get_solution(BacktrackingSolver(), lessons, days, *constraints)


def get_solution(solver, lessons, days, *constraints):
    problem = Problem(solver)
    problem.addVariables(lessons, days)
    for constraint in constraints:
        problem.addConstraint(constraint)
    return problem.getSolution()
